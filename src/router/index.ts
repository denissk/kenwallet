import { Express } from "express";
import { userRouter } from "./user.router"
import { loginRouter } from "./login.router";

export const initializerRouter = (app: Express) => {
    app.use('/api/login', loginRouter());
    app.use('/api/users', userRouter());
}