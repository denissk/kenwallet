import { Router } from "express";

import { create, list, currentUser } from "../controller/user.controller";
import { isAuthenticated } from '../middlewares/authentication.middleware';
const router = Router();

export const userRouter = () => {
    router.post('', create);
    router.get('', list);
    router.get('/currentUser', isAuthenticated, currentUser);
    return router;
}