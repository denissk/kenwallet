import { getRepository, getCustomRepository } from "typeorm";
import { User } from "../entities";

import UserRepository from '../repositories/userRepository';

interface UserBody {
    email: string,
    password: string,
    name: string
}

export const createUser = async (body: UserBody) => {
    const { email, password, name } = body;

    const userRepository = getRepository(User);

    const user = userRepository.create({
        email,
        password,
        name
    });

    await userRepository.save(user);

    return user;
}

export const listUser = async (page = 1) => {
    const userRepository = getCustomRepository(UserRepository);

    const users = await userRepository.findPaginated(page);

    return users;
}