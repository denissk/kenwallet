import "reflect-metadata";
import express from 'express';

import { initializerRouter } from './router';
import { connectDatabase } from "./database";

connectDatabase();

const app = express();

app.use(express.json());

initializerRouter(app);


export default app;